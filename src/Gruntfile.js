module.exports = function(grunt)
{
    grunt.template.addDelimiters('handlebars-like-delimiters', '{{', '}}');

    var grunt_config =
    {
        pkg : grunt.file.readJSON('package.json'),

        watch:
        {
            css:
            {
                files: ['./css/**/*.styl'],
                tasks: ['stylus']
            },

            js:
            {
                files: ['./js/**/*.js'],
                tasks: ['concat:dev']
            }
        },

        stylus:
        {
            compile:
            {
                options:
                {
                    compress: false,
                    paths: [],
                    relativeDest: '../app/css',
                    use:[],
                    import: []
                },

                files: {
                'style.css': './css/*.styl',
                }
            }
        },

        concat:
        {
            options:
            {
                separator: ';',
            },

            dist:
            {
                src: [
                    './js/libs/*.js',
                    './js/plugins/*.js',
                    './js/controllers/*.js',
                    './js/views/*.js',
                    './js/*.js'
                ],
                dest: './temp.js',
            },
            dev:
            {
                src: [
                    './js/libs/*.js',
                    './js/plugins/*.js',
                    './js/controllers/*.js',
                    './js/*.js',
                    './js/views/*.js'
                ],
                dest: '../app/js/scripts.js',
            }
        },

        uglify:
        {
            all:
            {
                files:
                {
                    '../app/js/scripts.js' : ['./temp.js']
                }
            }
        },

        browserSync:
        {
            dev:
            {
                bsFiles:
                {
                    src : [
                        '../app/css/*.css',
                        '../app/js/*.js',
                        '../app/*.html'
                    ]
                },
                options:
                {
                    watchTask: true,
                    proxy: "localhost:80/tegra-view/app"
                }
            }
        }
    };

    grunt.initConfig(grunt_config);

    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-stylus');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-browser-sync');

    grunt.registerTask('dev', ['stylus' , 'concat:dev', 'browserSync', 'watch']);
    grunt.registerTask('prod', ['stylus' , 'concat:dist', 'uglify']);
};
