(function(window)
{
    var helpers = {};

    helpers.scrollToByID = function(id, time)
    {
        time = (time==undefined) ? 100 : time;
        $('html,body').animate(
        {
            scrollTop: $("#" + id).offset().top
        }, time);
    };

    helpers.scrollToByPos = function(pos, callback)
    {
        if(!callback)
            callback = '';

        $('html,body').stop(true, true).animate(
        {
            scrollTop: pos
        }, 1000, 'easeInOutQuart', callback);
    };

    helpers.makeSlug = function(string)
    {
        var newstring = "";
        if(string)
        {
            string = string.toString();
            newstring = string.toLowerCase();
            newstring = newstring.replaceAll('+', ' ');
            newstring = $.trim(newstring);
            newstring = newstring.replaceAll(' ', '-');
            newstring = newstring.replaceAll('//', '/');
            newstring = accent_fold(newstring);

            function accent_fold(s)
            {
                var ret = '';
                var accent_map =
                {
                    'à': 'a', 'á': 'a', 'â': 'a', 'ã': 'a', 'ä': 'a', 'å': 'a', // a
                    'ç': 'c',                                                   // c
                    'è': 'e', 'é': 'e', 'ê': 'e', 'ë': 'e',                     // e
                    'ì': 'i', 'í': 'i', 'î': 'i', 'ï': 'i',                     // i
                    'ñ': 'n',                                                   // n
                    'ò': 'o', 'ó': 'o', 'ô': 'o', 'õ': 'o', 'ö': 'o', 'ø': 'o', // o
                    'ß': 's',                                                   // s
                    'ù': 'u', 'ú': 'u', 'û': 'u', 'ü': 'u',                     // u
                    'ÿ': 'y'                                                    // y
                };
                if (!s) { return ''; }

                for (var i = 0; i < s.length; i++)
                    ret += accent_map[s.charAt(i)] || s.charAt(i);

                return ret;
            }
        }
        return  newstring;
    }

    helpers.openPopUp = function (page, name, width, height)
    {
        w = formHelper.getWindowWidth();
        h = formHelper.getWindowHeight();
        half_w = w/2;
        half_h = h/2;
        height = (height == undefined) ? 800 :  height;
        width = (width == undefined) ? 500 :  width;
        height2 = height/2;
        width2 = width/2;
        half1 = half_h-height2;
        half2 = half_w-width2;
        popup = window.open(page, '_blank','height=' + height + ', width=' + width + ', top='+half1+', left='+half2+'');
        if(popup)
            if (window.focus) {popup.focus()}
        else
           alert('pop up foi bloqueado tente novamente.');
    };

    helpers.getSunday = function()
    {
        var curr = new Date(); // get current date
        var first = curr.getDate() - curr.getDay(); // First day is the day of the month - the day of the week
        var firstday = new Date(curr.setDate(first));

        return firstday.getFullYear() + '-' + (firstday.getMonth() + 1) + '-' + firstday.getDate();
    }

    helpers.slashDateFormat = function(date)
    {
        var aDate = date.split('-');
        return aDate[2] + '/' + aDate[1] + '/' + aDate[0];
    }

    helpers.hyphenDateFormat = function(date)
    {
        var aDate = date.split('/');
        return aDate[2] + '-' + aDate[1] + '-' + aDate[0];
    }

    helpers.validateCPF = function(cpf)
    {
        var Soma;
        var Resto;
        Soma = 0;
        cpf = cpf.replaceAll('.', '').replace('-', '');

    	if (cpf == "00000000000") return false;

    	for (i=1; i<=9; i++) Soma = Soma + parseInt(cpf.substring(i-1, i)) * (11 - i);
    	Resto = (Soma * 10) % 11;

        if ((Resto == 10) || (Resto == 11))  Resto = 0;
        if (Resto != parseInt(cpf.substring(9, 10)) ) return false;

    	Soma = 0;
        for (i = 1; i <= 10; i++) Soma = Soma + parseInt(cpf.substring(i-1, i)) * (12 - i);
        Resto = (Soma * 10) % 11;

        if ((Resto == 10) || (Resto == 11))  Resto = 0;
        if (Resto != parseInt(cpf.substring(10, 11) ) ) return false;
        return true;
    }

    helpers.validateCNPJ = function(cnpj)
    {
        cnpj = cnpj.replace(/[^\d]+/g,'');
        if(cnpj == '') return false;
        if (cnpj.length != 14)
            return false;

        // Elimina CNPJs invalidos conhecidos
        if (cnpj == "00000000000000" ||
            cnpj == "11111111111111" ||
            cnpj == "22222222222222" ||
            cnpj == "33333333333333" ||
            cnpj == "44444444444444" ||
            cnpj == "55555555555555" ||
            cnpj == "66666666666666" ||
            cnpj == "77777777777777" ||
            cnpj == "88888888888888" ||
            cnpj == "99999999999999")
            return false;

        // Valida DVs
        var tamanho = cnpj.length - 2
        var numeros = cnpj.substring(0,tamanho);
        var digitos = cnpj.substring(tamanho);
        var soma = 0;
        var pos = tamanho - 7;

        for (i = tamanho; i >= 1; i--)
        {
          soma += numeros.charAt(tamanho - i) * pos--;
          if (pos < 2) pos = 9;
        }

        var resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(0)) return false;

        tamanho = tamanho + 1;
        numeros = cnpj.substring(0,tamanho);
        soma = 0;
        pos = tamanho - 7;
        for (i = tamanho; i >= 1; i--)
        {
          soma += numeros.charAt(tamanho - i) * pos--;
          if (pos < 2)
                pos = 9;
        }

        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(1)) return false;

        return true;
    }

    helpers.validateEmail = function(email)
    {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        return re.test(email);
    }

    helpers.phoneMask = function (val) {
        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
    };
    helpers.phoneMaskOptions = {
      onKeyPress: function(val, e, field, options) {
          field.mask(helpers.phoneMask.apply({}, arguments), options);
        }
    };

    window.helpers = helpers;
} (window));

Array.prototype.clean = function(deleteValue)
{
    for (var i = 0; i < this.length; i++) {
        if (this[i] == deleteValue) {
            this.splice(i, 1);
            i--;
        }
    }
    return this;
};

String.prototype.replaceAll = function(target, replacement)
{
    return this.split(target).join(replacement);
};

jQuery.fn.sortElements = (function()
{
    var sort = [].sort;
    return function(comparator, getSortable)
    {
        getSortable = getSortable || function(){return this;};
        var placements = this.map(function()
        {
            var sortElement = getSortable.call(this),
                parentNode = sortElement.parentNode,
                nextSibling = parentNode.insertBefore(
                    document.createTextNode(''),
                    sortElement.nextSibling
                );

            return function()
            {
                if (parentNode === this) {
                    throw new Error(
                        "You can't sort elements if any one is a descendant of another."
                    );
                }
                parentNode.insertBefore(this, nextSibling);
                parentNode.removeChild(nextSibling);
            };
        });

        return sort.call(this, comparator).each(function(i){
            placements[i].call(getSortable.call(this));
        });
    };
})();
