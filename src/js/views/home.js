(function(window)
{
    var home = {};
    home.el = {
        body: $('body'),
        header: $('header'),
        footer: $('footer')
    }

    home.product = 'faro';
    home.tower = '1e2';
    home.floor = '14';
    home.canChange = true;
    home.products = ['faro', 'luce', 'sole'];

    home.standByTime = 240; // segundos;
    home.standByTimeout = null;
    home.homeIntervalDelay = 5;
    home.homeInterval = null;
    home.currentProduct = 0;


    home.init = function init()
    {
        home.onResize();
        home.setEvents();

        $(window).resize(function(e)
        {
            home.onResize(e);
        });

        home.activateHomeScreen();        

    };

    home.activateHomeScreen = function()
    {
        home.currentProduct = 0;
        home.changeProduct('faro');

        home.homeInterval = window.setInterval(function()
        {
            home.currentProduct = home.currentProduct + 1 < home.product.length - 1 ? home.currentProduct + 1 : 0;
            
            home.changeProduct(home.products[home.currentProduct]);
        }, home.homeIntervalDelay * 1000)
    }

    home.clearHomeScreenInterval = function() 
    {
        window.clearInterval(home.homeInterval);
    }

    
    home.resetStandBy = function()
    {
        home.clearHomeScreenInterval();   
        window.clearTimeout(home.standByTimeout);
        home.standByTimeout = null;

        home.standByTimeout = window.setTimeout(function()
        {
            console.log('standbyComplete');

            home.activateHomeScreen();

            if(!$('.panorama-panel').hasClass('closed'))
                home.togglePanel();

        }, home.standByTime * 1000);
    };

    home.setEvents = function setEvents()
    {
        // $('.panorama-image').off('mousedown').on('mousedown', function(e)
        // {
        //     home.closePanel();
        // });
        // $('.panorama-image').draggable({
        //     axis: 'x',
        //     scroll: false,
        //     stop: function() 
        //     {
        //         home.checkPanoramaPosition();

        //         window.setTimeout(home.checkPanoramaPosition, 500);                
        //     }
        // });

        $('html, body').off('mousemove').on('mousemove', function(e)
        {
            home.resetStandBy();
        });

        $('html, body').off('touchstart').on('touchstart', function(e)
        {
            home.resetStandBy();
        });

        $('html, body').off('mousedown').on('mousedown', function(e)
        {
            home.resetStandBy();
        });

        $('.panel-arrow').off('click').on('click', function()
        {
            home.togglePanel();            
        });

        $('.bt-product').off('click').on('click', function()
        {
            if(home.canChange)
                home.changeProduct($(this).data('product'));
        });

        $('.bt-tower').off('click').on('click', function()
        {
            if(home.canChange)
                home.changeView($(this).data('tower'));
        });

        $('.bt-floor').off('click').on('click', function()
        {
            if(home.canChange)
                home.changeView(home.tower, $(this).data('floor'));
        });
    };

    home.togglePanel = function()
    {
        $('.panorama-panel').toggleClass('closed');
        $('.panorama-image').toggleClass('expanded');
    }

    // home.checkPanoramaPosition = function()
    // {
    //     if($('.panorama-image').position().left > 0)
    //         $('.panorama-image').css({'left' : 0});

    //     if($('.panorama-image').position().left < -($('.panorama-image img').width() - $(window).width()))
    //         $('.panorama-image').css({'left' : -($('.panorama-image img').width() - $(window).width())});
    // }

    home.onScroll = function onScroll(e)
    {

    };

    home.onResize = function onResize(e)
    {
        $('.scale').css('transform', 'translateY(-50%) scale(' + $(window).height() / 2160 + ')');
        $('.logo-product').css('transform', 'scale(' + $(window).height() / 2160 + ')');
    };  
    
    home.changeProduct = function(nextProduct)
    {
        home.canChange = false;
        var $former = $('.panorama-image.' + home.product)
        $former.removeClass('current').addClass('former');

        home.product = nextProduct;
        home.tower = home.product == 'faro' ? '1e2' : '1';
        home.floor = '14';

        console.log(home.product, home.tower, home.floor);
        

        $('.panorama-image.' + home.product).addClass('current')

        window.setTimeout(function() 
        {
            $former.removeClass('former');
            home.changeView( $former.data('product') == 'faro' ? '1e2' : '1', '14', $former.data('product'));
            console.log($former.data('product'));
            
            home.canChange = true;
        }, 600);

        $('.map, .panorama-panel').removeClass('faro').removeClass('luce').removeClass('sole').addClass(home.product);

        $('.map-tower').hide();
        
        $('.map-tower.' + home.product).show();

        if(home.product == 'faro')
        {
            $('.map-tower').removeClass('tower-1e2').removeClass('tower-3e4').addClass('tower-1e2');
        } else {
            $('.map-tower').removeClass('tower-1').removeClass('tower-2').removeClass('tower-3').addClass('tower-1');
        }

        $('.floor-selector').removeClass('floor-14').removeClass('floor-12').removeClass('floor-10').removeClass('floor-8').removeClass('floor-6').removeClass('floor-4').addClass('floor-14')
                            .removeClass('sole').removeClass('luce').removeClass('faro').addClass(home.product);
    }

    home.changeView = function(tower, floor, product)
    {
        home.canChange = false;
        var $currentProduct = !product ? $('.panorama-image.current') : $('.panorama-image[data-product=' + product + ']');
        var $former = $currentProduct.find('.views img');
        $former.addClass('former');
        
        if(!product) 
        {
            home.tower = tower;
            home.floor = floor ? floor : '14';
        }

        console.log(home.product, home.tower, home.floor);

        var $img = $('<img class="current invisible" src="img/panoramas/' + (!product ? home.product : product) + '/' + tower + '-' + home.floor + '.jpg">');

        $currentProduct.find('.views').append($img);
        $currentProduct.find('.logo-product span').text(home.getCaption(!product ? home.product : product, tower, home.floor));

        $img.on('load', function()
        {
            $(this).removeClass('invisible');

            window.setTimeout(function() 
            {
                $former.remove();
                $img.removeClass('current');
                home.canChange = true;
            }, 600);
        });        

        if(!product)
        {
            if(home.product == 'faro')
            {
                $('.map-tower').removeClass('tower-1e2').removeClass('tower-3e4').addClass('tower-' + home.tower);
            } else {
                $('.map-tower').removeClass('tower-1').removeClass('tower-2').removeClass('tower-3').addClass('tower-' + home.tower);
            }

            $('.floor-selector').removeClass('floor-14').removeClass('floor-12').removeClass('floor-10').removeClass('floor-8').removeClass('floor-6').removeClass('floor-4').addClass(!floor ? 'floor-14' : 'floor-' + floor);
        }
    }

    home.getCaption = function(product, tower, floor)
    {
        var sTower = product != 'faro' ? product : '';
        return sTower.toUpperCase() + ' ' + floor + '° andar / Final ' + tower.toString().split('').join(' ');
    }

    home.closePanel = function() 
    {
        $('.panorama-panel').addClass('closed');
    }

    home.openPanel = function() 
    {
        $('.panorama-panel').removeClass('closed');
    }

    window.home = home;
} (window));
