(function(window)
{
    var main = {};
    main.el = {
        body: $('body'),
        header: $('header'),
        footer: $('footer')
    }

    main.init = function init()
    {
        main.onResize();
        main.setEvents();

        $(window).resize(function(e)
        {
            main.onResize(e);
        });
    };

    main.setEvents = function setEvents()
    {
        $(document).on("contextmenu", function (e) {
            console.log("ctx menu button:", e.which);

            // Stop the context menu
            e.preventDefault();
        });        
        
    }

    main.onScroll = function onScroll(e)
    {

    };

    main.onResize = function onResize(e)
    {

    };    

    window.main = main;
} (window));

$(document).ready(function($)
{
    main.init();
    home.init();
});
